cwlVersion: v1.0
$graph:
- class: CommandLineTool
  id: main
  baseCommand: Stars

  requirements:
    DockerRequirement:
      dockerPull: terradue/stars:1.0.0-beta.11

    InlineJavascriptRequirement: {}
  
    EnvVarRequirement:
      envDef:
        PATH: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
        AWS__ServiceURL: $(inputs.S3_SERVICE_URL)
        AWS__SignatureVersion: "2"
        AWS_ACCESS_KEY_ID: $(inputs.S3_ACCESS_KEY)
        AWS_SECRET_ACCESS_KEY: $(inputs.S3_SECRET_KEY)
        AWS__Region: $(inputs.S3_REGION)
    
    ResourceRequirement: {}
  
  arguments:
  - copy
  - -v
  - -r
  - '4'
  - -o
  - $( inputs.S3_BUCKET )
  - valueFrom: |
      ${
        if( !Array.isArray(inputs.wf_outputs) )
        {
            return inputs.wf_outputs.path + "/catalog.json";
        }
        var args=[];
        for (var i = 0; i < inputs.wf_outputs.length; i++)
        {
            args.push(inputs.wf_outputs[i].path + "/catalog.json");
        }
        return args;
      }
  inputs:
    S3_SERVICE_URL:
      type: string
    S3_ACCESS_KEY:
      type: string
    S3_SECRET_KEY:
      type: string
    S3_BUCKET:
      type: string
    S3_REGION:
      type: string
    wf_outputs: 
      type: Directory[]
  outputs: 
    catalog: 
      type: string
      outputBinding:
        outputEval: ${ return inputs.S3_BUCKET + "/catalog.json";} 
