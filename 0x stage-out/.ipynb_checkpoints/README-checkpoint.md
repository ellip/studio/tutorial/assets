


```console
cwltool --podman stage-out.cwl \
    --S3_REGION $S3_REGION \
    --S3_SECRET_KEY $S3_SECRET_KEY \
    --S3_SERVICE_URL https://s3.fr-par.scw.cloud/ \
    --S3_ACCESS_KEY $S3_ACCESS_KEY \
    --wf_outputs /workspace/app-burned-area/zf0qrwtb/ \
    --S3_BUCKET s3://data-61b8322b-7f08-4ae1-bac9-53e464792b32/results
```