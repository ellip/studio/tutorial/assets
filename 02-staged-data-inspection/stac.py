import enum
from typing import Iterator, List, Optional, Set
import sys, os
import re
from copy import deepcopy
import pystac
from pystac import Catalog, extensions
from pystac.asset import Asset
from pystac.extensions.eo import Band, EOExtension, AssetEOExtension
from pystac.extensions.raster import RasterBand, RasterExtension
from pystac.item import Item


def get_item(catalog) -> Item:

    cat = Catalog.from_file(catalog)

    try:

        collection = next(cat.get_children())
        item = next(collection.get_items())

    except StopIteration:

        item = next(cat.get_items())

    return item


def _get_assets(
    stac_item: pystac.Item,
    include: Optional[Set[str]] = None,
    exclude: Optional[Set[str]] = None,
    include_asset_types: Optional[Set[str]] = None,
    exclude_asset_types: Optional[Set[str]] = None,
) -> Iterator:
    """Get valid asset list.

    Args:
        stac_item (pystac.Item): STAC Item.
        include (Optional[Set[str]]): Only Include specific assets.
        exclude (Optional[Set[str]]): Exclude specific assets.
        include_asset_types (Optional[Set[str]]): Only include some assets base on their type.
        exclude_asset_types (Optional[Set[str]]): Exclude some assets base on their type.

    Yields
        str: valid STAC asset name.

    """
    for asset, asset_info in stac_item.get_assets().items():
        _type = asset_info.media_type

        if exclude and asset in exclude:
            continue

        if (
            _type
            and (exclude_asset_types and _type in exclude_asset_types)
            or (include and asset not in include)
        ):
            continue

        if (
            _type
            and (include_asset_types and _type not in include_asset_types)
            or (include and asset not in include)
        ):
            continue

        yield asset


class CommonBandName(str, enum.Enum):
    def __str__(self) -> str:
        return str(self.value)

    COASTAL = "coastal"
    BLUE = "blue"
    GREEN = "green"
    RED = "red"
    YELLOW = "yellow"
    PAN = "pan"
    REDEDGE = "rededge"
    REDEDGE70 = "rededge70"
    REDEDGE74 = "rededge74"
    REDEDGE78 = "rededge78"
    NIR = "nir"
    NIR08 = "nir08"
    NIR09 = "nir09"
    SWIR12 = "swir12"    
    CIRRUS = "cirrus"
    SWIR16 = "swir16"
    SWIR155 = "swir155"
    SWIR165 = "swir165"
    SWIR173 = "swir173"
    SWIR22 = "swir22"
    SWIR215 = "swir215"
    SWIR220 = "swir220"
    SWIR225 = "swir225"
    SWIR23 = "swir23"
    MWIR38 = "mwir38"
    LWIR = "lwir"
    LWIR9 = "lwir9"
    LWIR11 = "lwir11"
    LWIR12 = "lwir12"


def match_cbn(eoband: Band, cbn: CommonBandName) -> bool:

    if eoband.name == cbn.__str__():
        return True
    if eoband.common_name == cbn.__str__():
        return True

    return False


def match_cbn_freq(eoband: Band, cbn: CommonBandName) -> bool:

    split_cbn = re.split("(\d+)", cbn.__str__())
    if eoband.common_name == split_cbn[0] and eoband.center_wavelength is not None:
        freq = eoband.center_wavelength.__str__().replace(".", "")
        if freq.startswith(split_cbn[1]):
            return True

    return False


def get_new_asset(asset, eo_bands, indexes, raster_bands, cbn):

    new_asset = deepcopy(asset)
    new_eo_bands = [eo_bands[indexes[0]]]
    EOExtension.ext(new_asset).bands = new_eo_bands
    if raster_bands :
        new_raster_bands = [raster_bands[indexes[0]]]
        RasterExtension.ext(new_asset).bands = new_raster_bands

    return new_asset


def get_asset(item: Item, cbn: CommonBandName) -> Asset:
    """Get a single asset from item by its common band name convention"""

    for key, asset in item.get_assets().items():

        eo_bands = EOExtension.ext(asset).bands

        if eo_bands is None:
            continue
        raster_bands = RasterExtension.ext(asset).bands
        indexes = [i for i,band in enumerate(eo_bands) if match_cbn(band, cbn)]
        
        if len(indexes) == 0:
            continue

        new_asset = get_new_asset(asset, eo_bands, indexes, raster_bands, cbn)
        return new_asset
    
    for key, asset in item.get_assets().items():

        eo_bands = EOExtension.ext(asset).bands

        if eo_bands is None:
            continue
        raster_bands = RasterExtension.ext(asset).bands
        indexes = [i for i,band in enumerate(eo_bands) if match_cbn_freq(band, cbn)]
        
        if len(indexes) == 0:
            continue

        new_asset = get_new_asset(asset, eo_bands, indexes, raster_bands, cbn)
        return new_asset
    
    return None


def get_bands_common_names(item: Item) -> List[CommonBandName]:

    foundbands = []
    for key, asset in item.get_assets().items():
        eobands = EOExtension.ext(asset).bands
        if eobands is None:
            continue

        for eoband in eobands:
            if eoband.name in [x.lower() for x in CommonBandName.__members__.keys()]:
                foundbands.append(eoband.name)
                continue
            if eoband.common_name in [x.lower() for x in CommonBandName.__members__.keys()]:
                foundbands.append(eoband.common_name)
                continue
            freq = eoband.center_wavelength.__str__().replace(".", "")[0:2]
            if f"{eoband.common_name}{freq}" in [x.lower() for x in CommonBandName.__members__.keys()]:
                foundbands.append(f"{eoband.common_name}{freq}")
                continue
            freq = eoband.center_wavelength.__str__().replace(".", "")[0:3]
            if f"{eoband.common_name}{freq}" in [x.lower() for x in CommonBandName.__members__.keys()]:
                foundbands.append(f"{eoband.common_name}{freq}")
                continue

    return list(set(foundbands))