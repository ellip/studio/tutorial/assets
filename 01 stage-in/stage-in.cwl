$graph:
- class: CommandLineTool
  id: main
  requirements:
    DockerRequirement: 
      dockerPull: docker.io/terradue/stars:1.2.7
    InlineJavascriptRequirement: {}
  baseCommand: 
  - Stars
  - copy
  arguments:
  - --harvest
  - -v
  - -k
  - -r 
  - "4"
  - -rel 
  - valueFrom: ${ if (inputs.supplier) 
              { return ["-si", inputs.supplier]; }
              else {return '--empty'}
             }
  - valueFrom: ${ if (inputs.config_file)
              { return "-conf=$( inputs.config_file.path )"; }
              else {return '--empty'}
             }
  - valueFrom: ${ if (inputs.input_reference.split("#").length == 2) 
                {
                  var args=[];
                  var assets=inputs.input_reference.split("#")[1].split(',');
                  for (var i = 0; i < assets.length; i++)
                  {
                      args.push("-af " + assets[i]);
                  }
                  return args;
                }
                else {return '--empty'}
           }
  - -o 
  - ./
  - $( inputs.input_reference ) 
  inputs: 
    input_reference: 
      type: string
    config_file:
      type: File?
    supplier:
      type: string?
  outputs:
    staged:
      outputBinding:
        glob: .
      type: Directory
cwlVersion: v1.0