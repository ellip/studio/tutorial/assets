$graph:
- class: CommandLineTool
  id: main
  requirements:
    DockerRequirement: 
      dockerPull: docker.io/terradue/stars:1.0.0-beta.11
    InlineJavascriptRequirement: {}
  baseCommand: 
  - Stars
  - copy
  arguments:
  - -r 
  - "4"
  - -rel 
  - -o 
  - ./
  - $( inputs.input_reference ) 
  inputs: 
    input_reference:
      doc: A reference to an Openearch catalog entry or STAC Item
      label: A reference to an Opensearch catalog or STAC Item
      type: string
  outputs:
    staged:
      outputBinding:
        glob: .
      type: Directory
cwlVersion: v1.0
